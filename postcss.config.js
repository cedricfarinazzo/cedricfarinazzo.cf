const purgecss = require('@fullhuman/postcss-purgecss')({
    content: ['./hugo_stats.json'],
    defaultExtractor: content => {
      const els = JSON.parse(content).htmlElements;
      return [
        ...(els.tags || []),
        ...(els.classes || []),
        ...(els.ids || []),
      ];
    },
    fontFace: true,
    variables: true,
    safelist: ["tooltip", "valid-tooltip", "invalid-tooltip", "bs-tooltip-top", "bs-tooltip-right", "bs-tooltip-left", "bs-tooltip-bottom", "tooltip-inner", "fade", "show" ],
  });
  
  module.exports = {
    plugins: [purgecss]
  };