---
title: "Coding style checker"
link: https://gitlab.com/cedricfarinazzo/coding-style-checker
image: images/portfolio/projects/13-code.png
description: "A tools to check the coding style of your projects written in C."
tags: ["Python", "C", "Clang", "EPITA"]
fact: "A tools to check the coding style of your projects written in C."
weight: 488
---

For C projects at EPITA, we had to follow a strict coding style if we didn't want to have negative points.

So I decided to create this project and used it in the Gitlab CI pipeline of my projects. This allows me to check the coding style before submitting my project to my school, and thus lose less points on the coding style.