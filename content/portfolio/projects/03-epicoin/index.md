---
title: "Epicoin"
link: "https://github.com/EpicoinTM"
image: images/portfolio/projects/03-epicoin.png
description: "A cryptocurrency based on a blockchain written from scratch in C#"
tags: ["C#", "Blockchain", "Cryptocurrency"]
fact: "A cryptocurrency based on a blockchain written from scratch in C#."
weight: 498
---

Epicoin is a cryptocurrency based on a blockchain written from scratch in C#.

The system was decentralized on Node. Miners were able to mine to verify transactions in the network in exchange for a reward (Proof of Work).