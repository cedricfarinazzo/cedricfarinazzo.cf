---
title: "i3lock Fancy Multimonitor"
link: https://github.com/cedricfarinazzo/i3lock-fancy-multimonitor
image: images/portfolio/projects/10-i3.png
description: "Blurry lock screen for i3lock with multimonitor support."
tags: ["i3", "bash"]
fact: "Blurry lock screen for i3lock with multimonitor support."
weight: 490
---

It uses scrot to take a screenshot of the desktop, then ImageMagick blurs the image and adds a lock icon and text.

By using information from xrandr and basic math, this script supports multiple monitor setups, displaying the icon and text centered on all screens.