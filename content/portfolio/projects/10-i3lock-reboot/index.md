---
title: "i3lock reboot"
link: https://github.com/cedricfarinazzo/i3lock-reboot
image: images/portfolio/projects/10-i3.png
description: "A simple bash script to reboot i3lock (or another command) after a giving time."
tags: ["i3", "bash"]
fact: "A simple bash script to reboot i3lock (or another command) after a giving time."
weight: 491
---

A simple bash script to reboot i3lock (or another command) after a giving time.