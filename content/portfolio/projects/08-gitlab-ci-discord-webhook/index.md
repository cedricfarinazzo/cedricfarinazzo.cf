---
title: "Gitlab CI Discord Webhook"
link: https://gitlab.com/cedricfarinazzo/gitlab-ci-discord-webhook
image: images/portfolio/projects/08-gci-to-dgg.png
description: "Tools to send notifications of your gitlab pipeline to discord."
tags: ["Gitlab", "CI", "Bash"]
fact: "Tools to send notifications of your gitlab pipeline to discord."
weight: 493
---

This tools send a notification on Discord (Webhook) at the end of your gitlab pipeline.

|  |  |
|:-:|:-:|
| ![img](08-success.png) | ![img](08-failed.png)  |

