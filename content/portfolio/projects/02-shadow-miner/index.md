---
title: "Shadow Miner"
link: "https://github.com/cedricfarinazzo/ACCEr-project"
image: images/portfolio/projects/02-shadow-miner-icon.jpg
description: "A 3D video game made with Unity3D."
tags: ["Unity", "C#", "PHP", "MySQL", "EPITA"]
fact: "A 3D video game made with Unity3D."
weight: 499
---

![shadow-miner-img](02-shadow-miner-img.png)

Shadow Miner is a 3D video game made with the Unity3D game engine and coded in C # in 2018 for EPITA's 1st year project, with a website in PHP / MySQL connected to the game for the management of the accounts and the leaderboard.