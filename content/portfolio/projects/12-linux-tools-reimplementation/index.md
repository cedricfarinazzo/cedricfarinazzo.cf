---
title: "Linux tools reimplementation"
link: 
image: images/portfolio/projects/12-tux.png
description: "Reimplementation of many linux tools."
tags: ["C", "Shell", "Linux", "EPITA"]
fact: "Reimplementation of many linux tools."
weight: 489
---

During my years at EPITA, I had the opportunity to reimplement many linux tools in C and Shell, such as:
- cat
- make
- find
- malloc

Unfortunately, these projects are not open-source, as they are part of the EPITA curriculum.