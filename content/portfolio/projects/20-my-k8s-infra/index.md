---
title: "My Kubernetes infrastructure"
link: https://gitlab.com/sed-infra
image: images/portfolio/projects/20-k8s.png
description: "Cybersecurity courses project."
tags: ["DevOps", "Kubernetes", "k8s", "Debian", "Ansible", "Terraform"]
fact: "Cybersecurity courses project."
featured: true
weight: 481
---

In 2021, I created my first infrastructure based on a custom debian image, which allows me to automatically install it on servers.

I used k3s to create a Kubernetes cluster, where I host many services and projects, including the website.

All configuration is open source and stored on a dedicated gitlab organization `sed-infra` as I use GitOps to manage my Kubernetes cluster.
