---
title: "Tiger Compiler"
link:
image: images/portfolio/projects/15-tiger.jpg
description: "Implementation of a modern compiler in C++ following the book of Andrew W. Appel."
tags: ["C++", "Compiler", "Assembly", "EPITA"]
fact: "Implementation of a modern compiler in C++ following the book of Andrew W. Appel."
featured: true
weight: 486
---

During my years at EPITA, I had the opportunity to implement a compiler of the Tiger Language in C++.
I also use Flex and Bison to parse the language.

This project, which last 6 months, was divided in required parts:

- TC-1, Scanner and Parser
- TC-2, Building the Abstract Syntax Tree
- TC-3, Bindings
- TC-4, Type Checking
- TC-5, Translating to the High Level Intermediate Representation
- TC-6, Translating to the Low Level Intermediate Representation
- TC-7, Instruction Selection
- TC-8, Liveness Analysis
- TC-9, Register Allocation

There are also optional parts in the project, which we all did:

- TC-E, Computing the Escaping Variables
- TC-D, Removing the syntactic sugar from the Abstract Syntax Tree
- TC-I, Function inlining
- TC-B, Array bounds checking
- TC-A, Ad Hoc Polymorphism (Function Overloading)
- TC-O, Desugaring object constructs
- TC-C, Combine language extensions
- TC-L, LLVM IR
- TC-X, IA-32 Back End
- TC-Y, ARM Back End

As it was a very big project, we implement a lot of tests (around 12000) to ensure our compiler followed the Tiger Compiler Reference Manual.
This also us to have a working compiler at the end, and generate executable for many CPU architectures such as:
- i386: 32-bit Intel Architecture 
- ARM
- MISP

And some intermediate Representation language like:
- LLVM IR
- HIR (and run the program with HAVM)

Unfortunately, this project is not open-source, as it is part of the EPITA program. But the Tiger Compiler Reference Manual provided by the LRDE is available [here](https://www.lrde.epita.fr/~tiger/tiger.html).
You can also find the Tiger Project Assignment¶ [here](https://assignments.lrde.epita.fr/index.html)