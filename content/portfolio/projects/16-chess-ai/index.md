---
title: "Chess-AI"
link:
image: images/portfolio/projects/16-chess.jpg
description: "A Chess engine and AI written in C++"
tags: ["C++", "Chess", "Artificial Intelligence", "EPITA"]
fact: "A Chess engine and AI written in C++."
weight: 485
---

During my years at EPITA, I had the opportunity to do the Chess project.

This project aims to implement a Chess engine and a Chess AI written in C++.

Unfortunately, this project is not open-source, as it is part of the EPITA program.