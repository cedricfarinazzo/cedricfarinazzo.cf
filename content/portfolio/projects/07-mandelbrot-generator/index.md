---
title: "Mandelbrot Generator"
link: https://github.com/cedricfarinazzo/mandelbrot
image: images/portfolio/projects/07-mandelbrot.png
description: "The Mandelbrot fractal generated in Python with Multithreading."
tags: ["Python", "Multi Threading", "Image Processing"]
fact: "The Mandelbrot fractal generated in Python with Multithreading."
featured: true
weight: 494
---

This python project generate the Mandelbrot fractal.

As I wanted a very high precision in the image, the processing was very slow.
To fix this, I used the axis of symmetry of the Mandelbrot fractal to divided the process time by 2.
With 16 threads, I was still able to significantly reduce the process time.

At the end, this project was able to generate a very high precision image of the Mandelbrot fractal of 34MB. 

![4-thread](07-m11-thread.png)
