---
title: "Adaptative Neural Network"
link: https://github.com/cedricfarinazzo/AdaptativeNeuralNetwork 
image: images/portfolio/projects/09-ann.png
description: "A library containing multiple neural network models written in C"
tags: ["C", "Marchine Learning"]
fact: "A library containing multiple neural network models written in C"
featured: true
weight: 492
---
Adaptative Neural Network (ANN) is a static library containing multiple neural network models written in C.

### Models

#### Partially Connected Feedforward Neural Network (PCFNN) 

![pcfnn](09-pcfnnexample.gif)


The [Partially Connected Feedforward Neural Network (PCFNN)](https://cedricfarinazzo.github.io/ANN-doc/md_doc_models__p_c_f_n_n__r_e_a_d_m_e.html) is a feedforward neural network that can be used for classification and regression.

With it, you can create complex neural networks with partial or total connections between neuron layers.

At the end of the documentation, you will find an example neural network that learn the XOR function.

### Documentation

You can find the documentation of this project [here](https://cedricfarinazzo.github.io/ANN-doc/).

### Examples

[Here](https://github.com/cedricfarinazzo/ANNExample/tree/master/03-handwrittendigits_recognition) you will find an example projects using Adaptative Neural Network to do handwritten digits recognition.
