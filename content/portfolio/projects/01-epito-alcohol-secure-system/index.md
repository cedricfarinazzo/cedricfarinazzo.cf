---
title: "EPITO Alcohol Secure System"
link: "https://marketplace.atlassian.com/vendors/1017039"
image: images/portfolio/projects/01-epito.png
description: "Project done during the computer night in 2017."
tags: ["PHP", "HTML", "CSS", "MySQL"]
fact: "Project done during the computer night in 2017."
weight: 500
---


Project done during the computer night in 2017.

For our project subject, we have decided to focus on preventive action against driving after a drunken evening. To do this, we have created a website that allows the user to check if he has drunk too much to drive. The latter must indicate what he has drunk, his blood alcohol level is calculated and he is told whether he is allowed to drive or not. The parameters taken into account are his weight, his sex, if he is a young driver, without forgetting what he has drunk. The user also has the possibility of creating an account, in order to save his last evenings and his personal information so as not to have to enter them each time. An “Emergency numbers” page is also available, as well as prevention messages scattered on the site.