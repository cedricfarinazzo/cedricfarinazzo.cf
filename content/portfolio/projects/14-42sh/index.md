---
title: "42sh"
link:
image: images/portfolio/projects/14-42sh.png
description: "Implementation of a shell (bash-like) in C following the Shell Command Language."
tags: ["C", "Shell", "EPITA"]
fact: "Implementation of a shell (bash-like) in C following the Shell Command Language."
weight: 487
---

During my years at EPITA, I had the opportunity to carry out the ambitious and famous project: 42sh

This project aims to implement a shell in C following the Shell Command Language.

Unfortunately, this project is not open-source, as it is part of the EPITA program.