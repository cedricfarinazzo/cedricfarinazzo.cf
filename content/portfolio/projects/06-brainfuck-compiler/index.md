---
title: "Brainfuck Compiler"
link: https://github.com/cedricfarinazzo/BrainfuckCompiler
image: images/portfolio/projects/06-brainfuck.png
description: "A Brainfuck compiler written in OCaml."
tags: ["C", "Backup", "EPITA", "Python", "Django", "EPITA"]
fact: "A Brainfuck compiler written in OCaml."
weight: 495
---

This project is a Brainfuck compiler written in OCaml, just for fun :)

It can convert your Brainfuck program to an executable!
I also added the possibility to do functions in Brainfuck.