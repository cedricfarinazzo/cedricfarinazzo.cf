---
title: "My Website"
link: https://gitlab.com/cedricfarinazzo/cedricfarinazzo.cf
image: images/portfolio/projects/21-hugo.svg
description: "This site."
tags: ["Web"]
fact: "This site."
weight: 480
---

In 2022 I created this website using Hugo because it allows me to use Markdown.

It includes all my projects, blog posts and more.

This website is built on Gitlab CI and stored on my Kubernetes infrastructure inside Minio, an S3 server.