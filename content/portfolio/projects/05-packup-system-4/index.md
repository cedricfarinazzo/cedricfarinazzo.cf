---
title: "Packup System 4"
link: "https://gitlab.com/ps4-epita/packupsystem4"
image: images/portfolio/projects/05-ps4.png
description: "A incremental backup system with encryption and compression algorithm."
tags: ["C", "Backup", "EPITA", "Python", "Django", "EPITA"]
fact: "A incremental backup system with encryption and compression algorithm."
weight: 496
---

Packup System 4 is a file backup system written in C.
It allows you to save files (with permissions, modification dates, ...) incrementally with optional compression (Huffman, Lz7) and encryption (Vigenere, RSA, Elgamal, AES).
A website, made in python with the Django framework, acts as a cloud and allows you to list files from an archive.