---
title: "Configuration and management of a corporate network"
link:
image: images/portfolio/projects/19-cyber.jpg
description: "Cybersecurity courses project."
tags: ["Cybersecurity", "Machine Learning", "EPITA"]
fact: "Cybersecurity courses project."
featured: true
weight: 482
---

As part of the Systems, Networks and Security specialty at EPITA, I was able to create and manage a company network and I carried out many actions on it:

- Setting up LAN and DMZ isolation with pfSense
- Setting up, securing and administering workstations using the Active Directory (GPO, firewall)
- Implementation of a PKI and HTTP(S) introspection on pfSense (website blocking, anti-virus analysis)
- Setting up a server on the DMZ with Ansible: Deploying a Web server and a mail server
- Setting up a VPN server (OpenVPN and IPSEC / L2TP)
- Carrying out multiple attacks on the network (Network recognition, MITM, phishing mail with macro in a document, exploitation of a vulnerability in a PHP server, Reverse proxy via SSH)
- Strengthen network security based on the attacks we have done previously

Unfortunately, this project is not open-source, as it is part of the EPITA program.