---
title: "Vorpal OCR"
image: images/portfolio/projects/04-vorpal-ocr.png
description: "An Optical character recognition coded in C."
tags: ["C", "Marchine Learning", "Image Processing", "EPITA"]
fact: "An Optical character recognition coded in C."
weight: 497
---

Vorpal OCR is an Optical character recognition based on a neural network and coded in C with the SDL and GTK3 library.