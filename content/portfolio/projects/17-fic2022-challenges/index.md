---
title: "FIC 2022 - Challenges redaction"
link:
image: images/portfolio/projects/17-fic.png
description: "Creation of challenges for the FIC 2022"
tags: ["Cybersecurity", "FIC2022", "Machine Learning", "EPITA"]
fact: "Creation of challenges for the FIC 2022"
featured: true
weight: 484
---

In 2021, I create 2 challanges for the FIC2022 using Malware reverse engineering and Machine Learning on Ethernet frames.

Unfortunately, this project is not open-source, as it is part of the EPITA program.