---
title: "Hackathon Energy Data Hack 2021 - 3rd price"
link: https://github.com/atomesZ/hackaton-energy
image: images/portfolio/projects/18-hackathon.png
description: "Online hackathon around energy strategy and data organized by the French Ministry of the Armed Forces."
tags: ["Cybersecurity", "Machine Learning", "EPITA"]
fact: "Online hackathon around energy strategy and data organized by the French Ministry of the Armed Forces."
featured: true
weight: 483
---

In 2021, I participated in the competition organized by the Ministry of the Armed Forces around energy strategy and data.
31 teams of 5 members from IT schools took part in this competition.

The problem was to intercept a login and a password in an electrical network.
We used Machine Learning algorithms (Keras, KNN, Decision Tree, ...) to extract keystrokes from disturbances generated in the electrical network.

We managed to recover the login and password, which allowed us to win 3rd place.