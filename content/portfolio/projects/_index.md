---
title: Projects
date: 2020-01-07T15:00:28.528Z
link: Not applicable
image: https://i.pinimg.com/736x/0a/ef/a8/0aefa8713db736df259ed4bf4c962c71.jpg
description: A collection of projects
weight: 10
sitemap:
  priority: 0.5
  weight: 0.8
---
<!--

This page represents the landing page for "projects" section. It is also shown under the homepage header for "projects". It should be therefore relatively short and sweet.

\-->


A collection of projects authored by me, and shared with the community as a mostly open source project