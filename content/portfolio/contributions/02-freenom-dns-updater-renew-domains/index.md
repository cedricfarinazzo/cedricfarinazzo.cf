---
title: "Freenom dns updater"
link: "https://github.com/maxisoft/Freenom-dns-updater/pull/19"
image: images/portfolio/contributions/02-freenom.png
description: "Auto renew domains from Freenom in Python."
featured: true
tags: ["Python","freenom","autorenew"]
fact: "Auto renew domains from Freenom in Python."
weight: 499
---

Freenom dns updater is a tool written in python to update freenom's dns records

I added a feature to auto renew domains in Freenom using Python. This allows to renew domains just before they expired.
