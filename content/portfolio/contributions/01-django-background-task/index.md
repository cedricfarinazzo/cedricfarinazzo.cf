---
title: "Django Background Tasks Autoreload feature"
link: "https://github.com/arteria/django-background-tasks/pull/196"
image: images/portfolio/contributions/01-django.png
description: "Implement an autoreload system for the django package django-background-tasks."
featured: true
tags: ["Python","Django","django-background-tasks","autoreload"]
fact: "Implement an autoreload system for the django package django-background-tasks."
weight: 500
---

Django Background Task is a database-backed work queue for Django.

I added an auto-reload system to automatically reload the project configuration. This removes the need for developers to manually restart Django Background Task and thus improves productivity.
