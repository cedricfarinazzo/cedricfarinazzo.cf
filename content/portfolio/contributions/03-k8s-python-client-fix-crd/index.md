---
title: "Python Kubernetes Client - Custom resources support"
link: "https://github.com/kubernetes-client/python/pull/1806"
image: images/portfolio/contributions/03-k8s.png
description: "Support for Custom resources."
featured: true
tags: ["Python","Kubernetes"]
fact: "Support for Custom resources."
weight: 498
---

Kubernetes Python Client is a Python package to manages the Kubenetes API.

I added the support for Custom resources in the `apply` composant. This allows to do the same thing than `kubectl apply` on Custom resources but using Python.