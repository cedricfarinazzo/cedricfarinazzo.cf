---
title: Open Source Contributions
date: 2020-01-07T16:49:15.046Z
link: NA
image: https://st2.depositphotos.com/2065405/5718/i/950/depositphotos_57181367-stock-photo-word-cloud-open-source.jpg
description: A collection of projects to which I contributed, but did not create.
weight: 20
sitemap:
  priority: 0.5
  weight: 0.5
---
<!--

This page represents the landing page for "contributions" section. It is also shown under the homepage header for "contributions". It should be therefore relatively short and sweet.

-->

A collection of projects to which I contributed, but did not create. Contributing back to Open Source projects is a strong passion of mine, and requires a considerate approach to learn norms, standards and approach for each community for a successful merge!