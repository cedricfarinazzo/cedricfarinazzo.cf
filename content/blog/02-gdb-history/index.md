---
title: Enable GDB command history
date: "2020-09-23T23:42:42.000Z"
description: "How to enable gdb command history"
draft: false
featured: true
tags: [gdb,history]
---

![gdb logo](02-gdb-logo.jpg)


## Enabling Command History in GDB

GDB, the GNU Debugger, is a powerful tool for debugging applications written in C, C++, and other languages. However, by default, GDB does not record the commands you enter into the debugger. This can be frustrating if you need to refer back to previous commands or if you want to save your debugging session for later. Fortunately, it is possible to enable command history in GDB by modifying the configuration file called `.gdbinit`.

### How to Enable Command History in GDB

To enable command history in GDB, you need to add the following command to your `.gdbinit` file:

```gdb
set history save on
```

This will tell GDB to record your commands and save them to a file called `.gdb_history`. This file will be created in the same directory where you run GDB.

### Other Tips for Using GDB with Command History

There are a few other things you should know about using command history in GDB:

- If you want to see your command history while using GDB, you can use the `history` command. This will display a list of all the commands you have entered in the current session.

- You can use the `!` operator followed by a number to execute a previous command from your history. For example, `!42` will execute the 42nd command in your history.

- You can also use the `!` operator followed by a search string to execute the most recent command in your history that starts with that string. For example, `!break` will execute the most recent command that starts with `break`.

- It is a good idea to add `.gdb_history` to your `.gitignore` file if you are using GDB as part of a team. This will prevent the history file from being committed to your version control system and causing conflicts with other team members.

### Conclusion

Enabling command history in GDB is a simple but useful feature that can save you time and frustration when debugging your applications. Just remember to add `set history save on` to your `.gdbinit` file and to add `.gdb_history` to your `.gitignore` file if you are working on a team. 
Happy debugging!
