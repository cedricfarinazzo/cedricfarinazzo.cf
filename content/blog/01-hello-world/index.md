---
title: Hello World!
date: "2020-09-20T23:42:42.000Z"
description: "My first post!"
draft: false
featured: true
tags: [hello-world]
---

![Hello World](01-hello-world.jpg)

## Hey there!

Welcome to my new blog! I'm excited to share my thoughts and experiences with you all. In this post, I want to give a quick introduction to what you can expect from my blog in the future.

First and foremost, I plan on writing about a variety of subjects that interest me. This could be anything from programming and technology to personal development and productivity. I'm a big believer in continuous learning and growth, so I hope to share some of the things that I've learned along the way with you all.

In addition to writing about various subjects, I also want to share my open-source projects with you. If you're interested in checking out my work, be sure to follow me on [GitHub](https://github.com/cedricfarinazzo) and [GitLab](https://gitlab.com/cedricfarinazzo). You'll find all of my projects there, including some that are still in development.

This blog is hosted on my own personal Kubernetes cluster and is deployed using Gitlab CI. I'm always looking for new ways to improve my skills and knowledge, so hosting my own blog on a self-managed cluster is just one way I can do that.

Thanks for stopping by, and I hope you'll stick around for future posts!

Good night!